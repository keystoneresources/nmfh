$(document).ready(function() {

  //Media Queries-----------------------------------------------
	var queries = [
	  {
	    context: 'range_0',
	    match: function() {
	      slideDrawerEnable();
        document.documentElement.id = 'range_0';
        console.log('current range:', MQ.new_context);
	    },
	    unmatch: function() {
	    	slideDrawerDisable();
	    }
	  },
	  {
	    context: 'range_1',
	    match: function() {
	      slideDrawerEnable();
        document.documentElement.id = 'range_1';
        console.log('current range:', MQ.new_context);
	    },unmatch: function() {
	      slideDrawerDisable();
	    }
	  },
	  {
	    context: 'range_2',
	    match: function() {
	      slideDrawerEnable();
        document.documentElement.id = 'range_2';
        console.log('current range:', MQ.new_context);
	    },unmatch: function() {
	      slideDrawerDisable();
	    }
	  },
	  {
	    context: 'range_3',
	    match: function() {
	      slideDrawerDisable();
        document.documentElement.id = 'range_3';
        console.log('current range:', MQ.new_context);
	    },unmatch: function(){
	    }
	  },
	  {
	    context: 'range_4',
	    match: function() {
	      slideDrawerDisable();
        document.documentElement.id = 'range_4';
        console.log('current range:', MQ.new_context);
	    },unmatch: function(){
	    }
	  },
	  {
	    context: 'range_5',
	    match: function() {
	      slideDrawerDisable();
        document.documentElement.id = 'range_5';
        console.log('current range:', MQ.new_context);
	    }
	  }
	];
	MQ.init(queries);
	var range = MQ.new_context;
	
  function slideDrawerEnable(){
    var range = MQ.new_context;
    var trigger = '.snap-trigger-';
    var content = '#content_container';
    var drawer = '.snap-drawer-';
    //var contentBorder = '#content_push';
    $('.snap-drawers').show();
    $(content).addClass('active');
    $(trigger+'left').show();
    $(trigger+'right').show();
    $('#content_container.active').unbind();
    $(trigger+'left').click(function(){
      if($(content+'.open-left').length){
        $('.menu-icon').removeClass('is-clicked');
        $(content).stop().animate({marginLeft: '0px'}, function(){
          $(this).removeClass('open-left');
          $(drawer+'left').hide();
              //$(contentBorder).removeClass('open-left');
           $('#content_container.active').unbind();
        });
      }else{
        $(content).addClass('open-left').stop().animate({marginLeft: '250px'}, function(){
          $('.menu-icon').addClass('is-clicked');
           $('#content_container.active').click(function(){
            //alert("click");
            $(content).stop().animate({marginLeft: '0px'}, 250, function(){
              $('.menu-icon').removeClass('is-clicked');
              $(this).removeClass('open-left');
              //$(contentBorder).removeClass('open-left');
              $(drawer+'left').hide();
              $('#content_container.active').unbind();
            });//animte close
          });//click
          $(window).on('scroll', function() {
             $(content).stop().animate({marginLeft: '0px'}, 250, function(){
              $(this).removeClass('open-left');
              //$(contentBorder).removeClass('open-left');
              $(drawer+'left').hide();
              $('#content_container.active').unbind();
            });//animte close
          });
        });//animate
        //$(contentBorder).addClass('open-left');
        $(drawer+'left').show();
      }
    });
    $(trigger+'right').click(function(){
      if($(content+'.open-left').length){
        $(content).stop().animate({marginLeft: '0px'}, function(){
          $(this).removeClass('open-left');
          $(drawer+'left').hide();
          //$(contentBorder).removeClass('open-left');
           $('#content_container.active').unbind();
        });
      }
      if($(content+'.open-right').length){
        $(content).stop().animate({marginLeft: '0px'}, function(){
          $(this).removeClass('open-right');
          $(drawer+'left').hide();
              //$(contentBorder).removeClass('open-right');
           $('#content_container.active').unbind();
        });
      }else{
        $(content).addClass('open-right').stop().animate({marginLeft: '-250px'}, function(){
           $(drawer+'left').hide();
           $('#content_container.active').click(function(){
            $(content).stop().animate({marginLeft: '0px'}, 250, function(){
              $(this).removeClass('open-right');
              //$(contentBorder).removeClass('open-right');
              $(drawer+'right').hide();
              $('#content_container.active').unbind();
            });//animte close
          });//click
          $(window).on('scroll', function() {
             $(content).stop().animate({marginLeft: '0px'}, 250, function(){
              $(this).removeClass('open-right');
              //$(contentBorder).removeClass('open-right');
              $(drawer+'right').hide();
              $('#content_container.active').unbind();
            });//animte close
          });
        });//animate
        //$(contentBorder).addClass('open-right');
        $(drawer+'right').show();
      }
    });
  }//slideDrawerEnable

  function slideDrawerDisable(){
    var trigger = '.snap-trigger-';
    var content = '#content_container';
    //var contentBorder = '#content_push';
    $('.snap-drawers').hide();
    $(content).removeClass('active');
    $('#content_container.active').unbind();
  	$(trigger+'left').unbind().hide();
    $(trigger+'right').unbind().hide();
    if($(content+'.open-left').length){
      $(content).stop().animate({marginLeft: '0px'}, function(){
        $(this).removeClass('open-left');
        //$(contentBorder).removeClass('open-left');
      });
    }
    if($(content+'.open-right').length){
      $(content).stop().animate({marginLeft: '0px'}, function(){
        $(this).removeClass('open-right');
        //$(contentBorder).removeClass('open-right');
      });
    }
  }

  $("header").backstretch("/assets/img/elements/header_bg_img_1200x290@2x.jpg");
  if($('#range_2').length || $('#range_1').length || $('#range_0').length){
    $(".main-footer").backstretch("/assets/img/elements/footer_bg_img_mobile_640x640.jpg");
  }else{
    $(".main-footer").backstretch("/assets/img/elements/footer_bg_img_1200x290@2x.jpg");
  }
  if($('.page-38').length){
    var imageSrc = $('.parent-content img').attr('src');
    $(".parent-content").backstretch(imageSrc);
    $('.parent-content img').hide();
  }
  
  
  $('#datepicker').datepick({
    onSelect: setHash
  });
  function setHash(date) {
    var dayPicked = new Date(date).setHours(0,0,0,0);
    window.location.hash = dayPicked;
  }
  
  $('#end-date').datepick({
    dateFormat: 'yyyymmdd'
  });
  
  //hashchange for calendar
  if($('.parent-2').length){
    
    
/*
    $('a#day').click(function(e){
      e.preventDefault();
      $('.datepicker-container').removeClass('hidden').parent('li').addClass('active');
    });
*/
    
    $('.sidebar.calendar li').click(function(){
      $('.sidebar.calendar li').each(function(){
        $(this).removeClass('active');
      });
      $(this).addClass('active');
    });
    
    $(window).hashchange(function(e){
      var hash = location.hash.substring(1);
      var today = Date.today().setHours(0,0,0,0);
      var oneWeekFromNow = (7).days().fromNow();
      var oneMonthFromNow = (1).month().fromNow();
      var $resultsCount = 0;
      //console.log('today: '+today);
      //console.log('oneWeekFromNow: '+oneWeekFromNow);
      //console.log('oneMonthFromNow: '+oneMonthFromNow);
      //console.log('hash: '+hash);
      $('#no-events').addClass('hidden');
      if(hash == 'week'){
        $('#calendar-title').html('Upcoming Week');
        $('.datepicker-container').addClass('hidden').parent('li').removeClass('active');
        $('.calendar-block').each(function(){
          $(this).css({'float':'none'}).addClass('hidden');
          var eventStartDateUnix = $(this).data('date-start');
          var eventStartDateJS = new Date(eventStartDateUnix*1000);
          var comparison = eventStartDateJS.compareTo(oneWeekFromNow);
          if(comparison == -1 || comparison == 0){
            $(this).removeClass('hidden');
            $resultsCount ++;
          }
        });
        if($resultsCount == 1){
          $('.calendar-block').each(function(){
            $(this).css({'float':'left'});
          });
        }else if($resultsCount == 0){
          $('#no-events').removeClass('hidden');
        }
      }else if(hash == 'month'){
        $('#calendar-title').html('Upcoming Month');
        $('.datepicker-container').addClass('hidden').parent('li').removeClass('active');
        $('.calendar-block').each(function(){
          $(this).css({'float':'none'}).addClass('hidden');
          var eventStartDateUnix = $(this).data('date-start');
          var eventStartDateJS = new Date(eventStartDateUnix*1000);
          var comparison = eventStartDateJS.compareTo(oneMonthFromNow);
          if(comparison == -1 || comparison == 0){
            $(this).removeClass('hidden');
            $resultsCount ++;
          }
        });
        if($resultsCount == 1){
          $('.calendar-block').each(function(){
            $(this).css({'float':'left'});
          });
        }else if($resultsCount == 0){
          $('#no-events').removeClass('hidden');
        }
      }else if(hash == 'all'){
        $('#calendar-title').html('All Events');
        $('.datepicker-container').addClass('hidden').parent('li').removeClass('active');
        $('.calendar-block').each(function(){
          $(this).css({'float':'none'}).removeClass('hidden');
          $resultsCount ++;
        });
        if($resultsCount == 1){
          $('.calendar-block').each(function(){
            $(this).css({'float':'left'});
          });
        }else if($resultsCount == 0){
          $('#no-events').removeClass('hidden');
        }
      }else{
        var lastHash = hash.slice(-4);
        //console.log('lastHash: '+lastHash);
        if(lastHash == '0000'){
          $('.datepicker-container').removeClass('hidden').parent('li').addClass('active');
          $('.calendar-block').each(function(){
            $(this).css({'float':'none'}).addClass('hidden');
            var eventStartDateUnix = $(this).data('date-start');     
            var eventDay = new Date(eventStartDateUnix*1000).setHours(0,0,0,0);
            if( hash == eventDay ){
              $(this).removeClass('hidden');
              $resultsCount ++;
            }
          });
          if($resultsCount == 1){
            $('.calendar-block').each(function(){
              $(this).css({'float':'left'});
            });
          }else if($resultsCount == 0){
            $('#no-events').removeClass('hidden');
          }
        }
      }
    });

    $(window).hashchange();
    var hash = location.hash.substring(1);

    $('.cat-filter').click(function(e){  //filter on category
      var catName = $(this).html();
      var $resultsCount = 0;
      var catFilter = $(this).data('cat-filter');
      $('#calendar-title').html(catName);
      $('#no-events').addClass('hidden');
      $('.calendar-block').each(function(){
        $(this).css({'float':'none'}).addClass('hidden');
      });
      $(".calendar-block[data-category*='"+catFilter+"']").each(function(){
        $(this).removeClass('hidden');
        $resultsCount ++;
      });
      if($resultsCount == 1){
        $('.calendar-block').each(function(){
          $(this).css({'float':'left'});
        });
      }else if($resultsCount == 0){
        $('#no-events').removeClass('hidden');
      }
    });
  }//end if parent-2
  
/*
  calendar pagination code if needed...
  oh, and var originalArray = $("#job-search article"); goes at the beginning, forgot that.
  
  
  [4/15/15, 10:46:52 AM] Shonna G: This turns everything into pages
[4/15/15, 10:46:56 AM] Shonna G: //add 10 results per page
		var $children = $('#job-search article:visible');
    for(var i = 0, l = $children.length; i < l; i += 10) {
      $children.slice(i, i+10).wrapAll('<div class="page"></div>');
    }
[4/15/15, 10:47:07 AM] Shonna G: This creates pagination
[4/15/15, 10:47:09 AM] Shonna G: //Create pagination   
    $('#job-search .page').each(function(index){
    	var pageId = index + 1; 
    	$(this).attr('data-id', pageId); 
	    $('.paging ul').append('<li data-id="'+pageId+'">'+pageId+'</li>');
    }); 
    $('#job-search .page:first').show();
    $('.paging').find('li:first').addClass('active');
    $('.paging ul li').click(function(){
    	var pageId = $(this).attr('data-id');
	    $('.paging ul li').removeClass('active');
	    $('.paging ul li[data-id="'+pageId+'"]').addClass('active');
	    $('#job-search .page[data-id="'+pageId+'"]').show();
	    $('#job-search .page:not([data-id="'+pageId+'"])').hide();
	    //activateControls(); 
    });
[4/15/15, 10:47:26 AM] Shonna G: activateControls() is for first/last prev/next buttons, let me know if you need those
[4/15/15, 10:47:51 AM] Shonna G: This function updates the pagination
[4/15/15, 10:47:51 AM] Shonna G: function updatePagination(){
			$('#job-search .page').contents().unwrap();
			$("#job-search article").remove();
			$("#job-search .container").prepend(originalArray);
			
			var $children = $('#job-search article:visible');
	    for(var i = 0, l = $children.length; i < l; i += 10) {
	      $children.slice(i, i+10).wrapAll('<div class="page"></div>'); 
	    }
	    var pageAmount = $('#job-search .page').length;
	    var activeId = parseInt($('.paging ul li.active').attr('data-id'));
	    $('#job-search .page').each(function(index){
	    	var pageId = index + 1; 
	    	$(this).attr('data-id', pageId); 
	    });
	    $('.paging ul li').each(function(){
	    	var pagingId = parseInt($(this).attr('data-id'));
		    if(pagingId > pageAmount){
			    $(this).hide();
		    }else{
			    $(this).show();
		    }
	    });
	    if(activeId > pageAmount){
		    $('#job-search .page:first').show();
				$('.paging ul').find('li:first').addClass('active');
	    }else{
	    	$('.paging ul li:not([data-id="'+activeId+'"])').removeClass('active');
		    $('#job-search .page[data-id="'+activeId+'"]').show();
	    }
		}
[4/15/15, 10:48:35 AM] Shonna G: So you'll want to call updatePagination() when you do something that should change the amount of pages (like hide/show items)
*/
  
  $('.equal-height-container td').hover(function(){
    $(this).children('.overlay').show();
  }, function(){
    $(this).children('.overlay').hide();
  });
  
  $('.price-button').click(function() {       
    var priceValue = $(this).data('price');
    if(priceValue == 'other'){
      $('#recurring-price').val('');
    }else{
      $('#recurring-price').val(priceValue);
    }
  });
  
  $('#range_5 .slideshow-container').hover(function(){
    $('.slide-controler').each(function(){
      $(this).fadeIn();
    });
  }, function(){
    $('.slide-controler').each(function(){
      $(this).fadeOut();
    });
  });
  
  $('#price-other').click(function(){
    console.log('clicked');
    $('#recurring-price').delay(100).fadeOut(400).fadeIn(400).fadeOut(400).fadeIn(400).fadeOut(400).fadeIn(400);
  });
  
  if($('#pager .pager-link').length > 10){
    $('#pager').hide();
  }else{
    $('#pager').show();
  }
  
  if($('body.page-11').length || $('body.parent-11').length){
	  if ($('header .main-header .columnize ul > li').length %2 != 0){
		  $('header .main-header .columnize ul').append('<li><a style="cursor:default;pointer-events:none;opacity:0;"></</li>');
		}
  } 

}); //End Document Ready