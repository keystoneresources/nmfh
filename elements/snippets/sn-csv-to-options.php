<?php
$optionsArray = explode(",",$csv);
  $output = '';
  foreach($optionsArray as $option){
    $hmacVerificationOutput = $modx->runSnippet(
      "hmacVerification",
      array(
        "var_name" => $var_name,
        "var_value" => $option,
        "var_code" => $var_code,
        "for_value" => true
      )
    );
    $output .= '<option value="'.$hmacVerificationOutput.'">'.$option.'</option>';
  }
  return $output;