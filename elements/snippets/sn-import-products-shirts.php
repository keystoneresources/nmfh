<?php
$modx->log(modX::LOG_LEVEL_ERROR,'Product importer started');
$returnMessage = 'Product importer started<br>';

$file = $hook->getValue('shirts');
$source_file = $file['tmp_name'];

//$source_file = $modx->config['base_path']."assets/import/shirt-import-20151202.csv";

$max_line_length = 0; // maximum line length for the csv file
$header = NULL; // Initialize header
$data = array(); // Initialize data array
$today = getdate();
$createdDateTime = $today[0]; // Unix Time

// Load csv and create an associative array to allow column values to appear in any order and be found by text column key
ini_set('auto_detect_line_endings', TRUE);
if (file_exists($source_file) && ($file_handle = fopen($source_file, 'r')) !== FALSE){
	while (($data = fgetcsv($file_handle, $max_line_length, ',' )) !== FALSE){
	  if(!$header){ //get header array
	    $header = $data; // Assume the first row is the header
	    $columns = array(
	      "name",
	      "category",
	      "code",
	      "price",
	      "orderable",
	      "ship_weight",
	      "taxable",
	      "caption",
	      "options_title",
	      "options",
	      "options_title_2",
	      "options_2",
	      "image_url");
	    foreach($columns as $column){
  	    if(!in_array($column, $header)){ //test header names
    	    $returnMessage .= 'Import Failed! There is no header item for '.$column.' in the uploaded CSV. Please reformat the CSV and try again.<br>';
    	    $hook->addError('returnMessage',$returnMessage);
  		    fclose($file_handle);
  				unlink($source_file);
  		    return false;
		    }//end if
	    }//end foreach column
	  }else{
	    //note: if the csv is malformed, this line will explode into terribleness.
	    $row_buffer[] = array_combine($header, $data);
		}
	}
} else {
  $returnMessage .= 'An error occurred opening the CSV! Please check that it is properly encoded.<br>';
  $hook->addError('returnMessage',$returnMessage);
	fclose($file_handle);
  unlink($source_file);
	return false;
}//end loading csv array

foreach ($row_buffer as $row){ // test for empty rows
	if(empty($row['name'])){
  	$returnMessage .= 'Import Failed! There are one or more empty rows in the uploaded CSV. Please reformat the CSV and try again.<br>';
    $hook->addError('returnMessage',$returnMessage);
		fclose($file_handle);
		unlink($source_file);
		return false; // throw up if there are any empty rows
	}
}

foreach ($row_buffer as $row){ // start import

	$document = $modx->getObject('modResource', array('longtitle' => $row['code'])); // look for existing document

	if(!empty($document)){ // update existing document
		//could put update script here, but not in current scope

	}else{ // create new document

    $newDocument = $modx->newObject('modResource');

  	$alias = $newDocument->cleanAlias($row['name']); 	
    $r = $modx->getObject('modResource', array('alias'=>$alias));
    if($r){ // alias in use
      $modx->log(modX::LOG_LEVEL_ERROR,'alias in use');
      $i = 1;
      for($i = 1; $i < 100; $i++){
        $r = $modx->getObject('modResource', array('alias'=>$alias.$i));
        if (!$r) { // Found an unused one
          $alias .= $i;
          break 1;
        }
      }
    }				
		$newDocument->set('type', 'document');
		$newDocument->set('contentType', 'text/html');
		$newDocument->set('pagetitle', $row['name']);
		$newDocument->set('longtitle', $row['code']);
		$newDocument->set('description', '');
		$newDocument->set('alias', $alias);
		$newDocument->set('link_attributes', '');
		$newDocument->set('published', 1);
		$newDocument->set('pub_date', 1);
		$newDocument->set('unpub_date', 0);
		$newDocument->set('parent', $row['category']);
		$newDocument->set('isfolder', 0);
		$newDocument->set('introtext', '');
		$newDocument->set('content', $row['caption']);
		$newDocument->set('richtext', 1);
		$newDocument->set('template', 14);
		$newDocument->set('menuindex', 1);
		$newDocument->set('searchable', 1);
		$newDocument->set('cacheable', 1);
		$newDocument->set('createdby', $modx->user->get('id'));
		$newDocument->set('createdon',$createdDateTime);
		$newDocument->set('editedby', '');
		$newDocument->set('editedon', '');
		$newDocument->set('deleted', '');
		$newDocument->set('deletedon', '');
		$newDocument->set('deletedby', '');
		$newDocument->set('publishedon', '');
		$newDocument->set('publishedby', '');
		$newDocument->set('menutitle', '');
	  $newDocument->set('donthit', '');
		$newDocument->set('haskeywords', '');
		$newDocument->set('hasmetatags', '');
		$newDocument->set('privateweb', '');
		$newDocument->set('privatemgr', '');
		$newDocument->set('content_dispo', '');
		$newDocument->set('hidemenu', '');
		$newDocument->set('class_key', 'modDocument');
		$newDocument->set('context_key', 'web');
		$newDocument->set('content_type', 1);
		$newDocument->set('show_in_tree', 0);
		if (!$newDocument->save()){ // Save the document
  		$returnMessage .= 'An error occurred while saving '.$row['name'].'<br>';
      $hook->addError('returnMessage',$returnMessage);
			fclose($file_handle);
      unlink($source_file);
	  	return false;
		}

		$modx->log(modX::LOG_LEVEL_ERROR,'document saved: '.$row['name'].' - Code = '.$row['code']);

		//update TV's
		$page = $modx->getObject('modResource',array('longtitle' => $row['code']));
		
		if (!$page->setTVValue('product_code', $row['code'])) {
  		$returnMessage .= 'There was a problem saving your TV (product_code).<br>';
      $hook->addError('returnMessage',$returnMessage);
			fclose($file_handle);
	    unlink($source_file);
			return false;
		}
		if (!$page->setTVValue('product_price', $row['price'])) {
  		$returnMessage .= 'There was a problem saving your TV (product_price).<br>';
      $hook->addError('returnMessage',$returnMessage);
			fclose($file_handle);
	    unlink($source_file);
			return false;
		}
		if (!$page->setTVValue('product_orderable', $row['orderable'])) {
  		$returnMessage .= 'There was a problem saving your TV (product_orderable).<br>';
      $hook->addError('returnMessage',$returnMessage);
			fclose($file_handle);
	    unlink($source_file);
			return false;
		}
		if (!$page->setTVValue('product_weight', $row['ship_weight'])) {
  		$returnMessage .= 'There was a problem saving your TV (product_weight).<br>';
      $hook->addError('returnMessage',$returnMessage);
			fclose($file_handle);
	    unlink($source_file);
			return false;
		}
		if (!$page->setTVValue('product_main_image', $row['image_url'])) {
  		$returnMessage .= 'There was a problem saving your TV (product_main_image).<br>';
      $hook->addError('returnMessage',$returnMessage);
			fclose($file_handle);
	    unlink($source_file);
			return false;
		}
		if (!$page->setTVValue('product_taxable', $row['taxable'])) {
  		$returnMessage .= 'There was a problem saving your TV (product_taxable).<br>';
      $hook->addError('returnMessage',$returnMessage);
			fclose($file_handle);
	    unlink($source_file);
			return false;
		}
		if (!$page->setTVValue('product_options_title', $row['options_title'])) {
  		$returnMessage .= 'There was a problem saving your TV (product_options_title).<br>';
      $hook->addError('returnMessage',$returnMessage);
			fclose($file_handle);
	    unlink($source_file);
			return false;
		}
		if (!$page->setTVValue('product_options', $row['options'])) {
  		$returnMessage .= 'There was a problem saving your TV (product_options).<br>';
      $hook->addError('returnMessage',$returnMessage);
			fclose($file_handle);
	    unlink($source_file);
			return false;
		}
		if (!$page->setTVValue('product_options_2_title', $row['options_title_2'])) {
  		$returnMessage .= 'There was a problem saving your TV (product_options_2_title).<br>';
      $hook->addError('returnMessage',$returnMessage);
			fclose($file_handle);
	    unlink($source_file);
			return false;
		}
		if (!$page->setTVValue('product_options_2', $row['options_2'])) {
  		$returnMessage .= 'There was a problem saving your TV (product_options_2).<br>';
      $hook->addError('returnMessage',$returnMessage);
			fclose($file_handle);
	    unlink($source_file);
			return false;
		}
		if($modx->getCacheManager()){ // Clear the cache
	    $modx->cacheManager->refresh();
		}
	} // close else ($newDocument)
} // close foreach loop

fclose($file_handle);
unlink($source_file);
$modx->log(modX::LOG_LEVEL_ERROR,'Import Successful!');
return true;