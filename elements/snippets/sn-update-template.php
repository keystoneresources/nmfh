<?php
$modx->log(modX::LOG_LEVEL_ERROR,'Template updater started');

$parentResource = $modx->getObject('modResource', array('id' => $parentID));
$children = $parentResource->getMany('Children');
if (!$children) { return false; }
foreach ($children as $child) {
  $child->set('template', $newTemplate);
  if ($child->save() == false){ // Save the document
			$modx->log(modX::LOG_LEVEL_ERROR,'An error occurred while saving '.$child->get('id'));
	  	return false;
		}
}

$modx->log(modX::LOG_LEVEL_ERROR,'Template Update Successful!');
return true;