<?php
$aliasArray = explode(", ", $aliasArray);
  $pagetitles = [];
  foreach($aliasArray as $alias){
    $document = $modx->getObject(modResource, array('alias' => $alias));
    $pagetitle = $document->get('pagetitle');
    array_push($pagetitles, $pagetitle);
  }
  //$modx->log(modX::LOG_LEVEL_ERROR,'$aliasArray: '.print_r($pagetitles, true));
  $output = implode(', ', $pagetitles);
  if($output){
    return $output;
  }else{
    return true;
  }