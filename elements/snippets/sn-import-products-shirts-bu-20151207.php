<?php
$modx->log(modX::LOG_LEVEL_ERROR,'Product importer started');
$source_file = $modx->config['base_path']."assets/import/shirt-import-20151202.csv";
$max_line_length = 0; // maximum line length for the csv file
$header = NULL; // Initialize header
$data = array(); // Initialize data array
$today = getdate();
$createdDateTime = $today[0]; // Unix Time

// Load csv and create an associative array to allow column values to appear in any order and be found by text column key
ini_set('auto_detect_line_endings', TRUE);
if (file_exists($source_file) && ($file_handle = fopen($source_file, 'r')) !== FALSE){
	while (($data = fgetcsv($file_handle, $max_line_length, ',' )) !== FALSE){
	  if(!$header){ //get header array
	    $header = $data; // Assume the first row is the header
	    $columns = array(
	      "name",
	      "category",
	      "code",
	      "price",
	      "orderable",
	      "ship_weight",
	      "taxable",
	      "caption",
	      "options_title",
	      "options",
	      "options_title_2",
	      "options_2",
	      "image_url");
	    foreach($columns as $column){
  	    if(!in_array($column, $header)){ //test header names
  	    	$modx->log(modX::LOG_LEVEL_ERROR,'Import Failed! There is no header item for '.$column.' in the uploaded CSV. Please reformat the CSV and try again.');
  		    fclose($file_handle);
  				unlink($source_file);
  		    return false;
		    }//end if
	    }//end foreach column
	  }else{
	    //note: if the csv is malformed, this line will explode into terribleness.
	    $row_buffer[] = array_combine($header, $data);
		}
	}
} else {
	$modx->log(modX::LOG_LEVEL_ERROR,'An error occurred opening the CSV! Please check that it is properly encoded.');
	fclose($file_handle);
  unlink($source_file);
	return false;
}//end loading csv array

foreach ($row_buffer as $row){ // test for empty rows
	if(empty($row['name'])){
		$modx->log(modX::LOG_LEVEL_ERROR,'Import Failed! There are one or more empty rows in the uploaded CSV. Please reformat the CSV and try again.');
		fclose($file_handle);
		unlink($source_file);
		return false; // throw up if there are any empty rows
	}
}

foreach ($row_buffer as $row){ // start import
	$modx->log(modX::LOG_LEVEL_ERROR,'Import loop started for: '.$row['code']);
	$document = $modx->getObject('modResource', array('longtitle' => $row['code'])); // look for existing document

	if(!empty($document)){ // update existing document
		$modx->log(MODX_LOG_LEVEL_ERROR, "This product already exists! (".$row['code'].")");
		
	}else{ // create new document

  	$modx->log(modX::LOG_LEVEL_ERROR,'Document does not exist: '.$row['code']);

    //set category parent
  	if($row['category'] == "Premium Dress Shirts - Big & Tall"){
    	$cat_id = 354;
    	$modx->log(modX::LOG_LEVEL_ERROR,'$cat_id: '.$cat_id);
  	}else if($row['category'] == "Premium Dress Shirts"){
    	$cat_id = 353;
    	$modx->log(modX::LOG_LEVEL_ERROR,'$cat_id: '.$cat_id);
  	}


    $newDocument = $modx->newObject('modResource');
    $modx->log(modX::LOG_LEVEL_ERROR,'newDocument Started');

  	$alias = $newDocument->cleanAlias($row['name']);
  	$modx->log(modX::LOG_LEVEL_ERROR,'og $alias: '.$alias);
  	

    $r = $modx->getObject('modResource', array('alias'=>$alias));
    if($r){ // alias in use
      $modx->log(modX::LOG_LEVEL_ERROR,'alias in use');
      $i = 1;
      for($i = 1; $i < 100; $i++){
        $r = $modx->getObject('modResource', array('alias'=>$alias.$i));
        if (!$r) { // Found an unused one
          $alias .= $i;
          $modx->log(modX::LOG_LEVEL_ERROR,'$alias = '.$alias);
          break 1;
        }
      }
    }
    $modx->log(modX::LOG_LEVEL_ERROR,'new $alias: '.$alias);
				
		$newDocument->set('type', 'document');
		$newDocument->set('contentType', 'text/html');
		$newDocument->set('pagetitle', $row['name']);
		$newDocument->set('longtitle', $row['code']);
		$newDocument->set('description', '');
		$newDocument->set('alias', $alias);
		$newDocument->set('link_attributes', '');
		$newDocument->set('published', 1);
		$newDocument->set('pub_date', 1);
		$newDocument->set('unpub_date', 0);
		$newDocument->set('parent', $cat_id);
		$newDocument->set('isfolder', 0);
		$newDocument->set('introtext', '');
		$newDocument->set('content', $row['caption']);
		$newDocument->set('richtext', 1);
		$newDocument->set('template', 14);
		$newDocument->set('menuindex', 1);
		$newDocument->set('searchable', 1);
		$newDocument->set('cacheable', 1);
		$newDocument->set('createdby', $modx->user->get('id'));
		$newDocument->set('createdon',$createdDateTime);
		$newDocument->set('editedby', '');
		$newDocument->set('editedon', '');
		$newDocument->set('deleted', '');
		$newDocument->set('deletedon', '');
		$newDocument->set('deletedby', '');
		$newDocument->set('publishedon', '');
		$newDocument->set('publishedby', '');
		$newDocument->set('menutitle', '');
	  $newDocument->set('donthit', '');
		$newDocument->set('haskeywords', '');
		$newDocument->set('hasmetatags', '');
		$newDocument->set('privateweb', '');
		$newDocument->set('privatemgr', '');
		$newDocument->set('content_dispo', '');
		$newDocument->set('hidemenu', '');
		$newDocument->set('class_key', 'modDocument');
		$newDocument->set('context_key', 'web');
		$newDocument->set('content_type', 1);
		$newDocument->set('show_in_tree', 0);
		if (!$newDocument->save()){ // Save the document
			$modx->log(modX::LOG_LEVEL_ERROR,'An error occurred while saving '.$row['name']);
			fclose($file_handle);
      unlink($source_file);
	  	return false;
		}

		$modx->log(modX::LOG_LEVEL_ERROR,'document saved');

		//update TV's
		$page = $modx->getObject('modResource',array('longtitle' => $row['code']));
		
		if (!$page->setTVValue('product_code', $row['code'])) {
			$modx->log(modX::LOG_LEVEL_ERROR,'There was a problem saving your TV (product_code).');
			fclose($file_handle);
	    unlink($source_file);
			return false;
		}
		if (!$page->setTVValue('product_price', $row['price'])) {
			$modx->log(modX::LOG_LEVEL_ERROR,'There was a problem saving your TV (product_price).');
			fclose($file_handle);
	    unlink($source_file);
			return false;
		}
		if (!$page->setTVValue('product_orderable', $row['orderable'])) {
			$modx->log(modX::LOG_LEVEL_ERROR,'There was a problem saving your TV (product_orderable).');
			fclose($file_handle);
	    unlink($source_file);
			return false;
		}
		if (!$page->setTVValue('product_weight', $row['ship_weight'])) {
			$modx->log(modX::LOG_LEVEL_ERROR,'There was a problem saving your TV (product_weight).');
			fclose($file_handle);
	    unlink($source_file);
			return false;
		}
		if (!$page->setTVValue('product_main_image', $row['image_url'])) {
			$modx->log(modX::LOG_LEVEL_ERROR,'There was a problem saving your TV (product_main_image).');
			fclose($file_handle);
	    unlink($source_file);
			return false;
		}
		if (!$page->setTVValue('product_taxable', $row['taxable'])) {
			$modx->log(modX::LOG_LEVEL_ERROR,'There was a problem saving your TV (product_taxable).');
			fclose($file_handle);
	    unlink($source_file);
			return false;
		}
		if (!$page->setTVValue('product_options_title', $row['options_title'])) {
			$modx->log(modX::LOG_LEVEL_ERROR,'There was a problem saving your TV (product_options_title).');
			fclose($file_handle);
	    unlink($source_file);
			return false;
		}
		if (!$page->setTVValue('product_options', $row['options'])) {
			$modx->log(modX::LOG_LEVEL_ERROR,'There was a problem saving your TV (product_options).');
			fclose($file_handle);
	    unlink($source_file);
			return false;
		}
		if (!$page->setTVValue('product_options_2_title', $row['options_title_2'])) {
			$modx->log(modX::LOG_LEVEL_ERROR,'There was a problem saving your TV (product_options_2_title).');
			fclose($file_handle);
	    unlink($source_file);
			return false;
		}
		if (!$page->setTVValue('product_options_2', $row['options_2'])) {
			$modx->log(modX::LOG_LEVEL_ERROR,'There was a problem saving your TV (product_options_2).');
			fclose($file_handle);
	    unlink($source_file);
			return false;
		}
		if($modx->getCacheManager()){ // Clear the cache
	    $modx->cacheManager->refresh();
		}
	} // close else ($newDocument)
} // close foreach loop

fclose($file_handle);
unlink($source_file);
$modx->log(modX::LOG_LEVEL_ERROR,'Import Successful!');
return true;