<?php
$template = $resource->get('template');
  if($template == 9){//check to make sure we have a calendar template before triggering plugin.
    // Log all available properties of the $resource for testing only
    //$modx->log(MODX_LOG_LEVEL_ERROR, print_r($resource->toArray(),true) );// Log all available properties of the $resource

    $cal_end_date = $resource->get('tv15');// get current cal_end_date
    $cal_start_date = $resource->get('tv14');// if there is none set, get the cal_start_date

    if($cal_end_date){
      $resource->set('unpub_date', $cal_end_date);// if cal_end_date is set then write this setting to the unpublish date and save
    }else{// if cal_end_date is not set calculate one day after cal_start_date and set unpublish to that date and save.
      $cal_start_date_time = strtotime($cal_start_date);
      $unpublish_date = date('Y-m-d H:i:s', strtotime('+1 day', $cal_start_date_time));
      $resource->set('unpub_date', $unpublish_date);
    }
  }

  return true;